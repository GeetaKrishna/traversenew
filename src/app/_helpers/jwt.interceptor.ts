import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {
  constructor() { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to api url
    // const currentUser = this.authentication.currentUserValue;
    // const isLoggedIn = currentUser && currentUser.token;
    // console.log(currentUser);
    const isLoggedIn = localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser')).token;

    const isApiUrl = request.url.startsWith(environment.apiUrl);
    if (isLoggedIn && isApiUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${JSON.parse(localStorage.getItem('currentUser')).token}`
        }
      });
    }
    console.log(request);
    console.log(request.body);

    return next.handle(request);
  }

}
