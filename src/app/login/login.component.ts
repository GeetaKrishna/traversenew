import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  login;

  constructor(private renderer: Renderer2, private authentication: LoginService, private router: Router) { }

  signIn() {
    this.authentication.login('ngeesidi', 'Miracle@1234').subscribe((data) => {
      console.log(data);
      this.router.navigateByUrl('/dashboard');
    }, (err) => {
      console.log(err);
    });
  }

  ngOnInit() {
    this.renderer.addClass(document.body, 'login-body');
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'login-body');
  }

}
