import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) { }

  login(username: string, password: string) {
    console.log({ username, password });
    return this.http.post<any>(`${environment.apiUrl}/authenticate`,
      { username, password },
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), observe: 'response' })
      .pipe(map(user => {
        // // login successful if there's a jwt token in the response
        // console.log(user)
        // if (user && user.headers.get('authorization')) {
        //   console.log('user')

        //   // store user details and jwt token in local storage to keep user logged in between page refreshes
        //   console.log(this.jwtHelper.decodeToken(user.headers.get('authorization').split(" ")[1]))
        localStorage.setItem('currentUser', JSON.stringify({ token: user.headers.get('authorization').split(' ')[1] }));
        localStorage.setItem('access_token', user.headers.get('authorization').split(' ')[1]);
        localStorage.setItem('userName', this.jwtHelper.decodeToken(user.headers.get('authorization').split(' ')[1]).sub);
        // this.currentUserSubject.next({ 'token': user.headers.get('authorization').split(" ")[1] });
        return user;
      }));
  }

  getUserId(username) {
    return this.http.get(`${environment.apiUrl}/users/name/${username}`)
      .pipe(map(user => {
        let t: any = {
          userId: '',
          bloodGroup: '',

        };
        t = user;
        localStorage.setItem('userId', t.userId);
        t.password = undefined;
        // localStorage.setItem('currentUser', JSON.stringify({ token: currentUser.token, loggedInUser: user, role: user.role }));
        localStorage.setItem('loggedInUser', JSON.stringify(user));
        localStorage.setItem('bloodGroup', t.bloodGroup);

        return user;
      }));

  }

  getPatientByUserId() {
    const relation = 'Self';
    let p: any = {};
    // tslint:disable-next-line: radix
    return this.http.get(`${environment.apiUrl}/patients/u/${parseInt(localStorage.getItem('userId'))}/${relation}`)
      .pipe(map(patient => {
        p = patient;
        localStorage.setItem('patientData', JSON.stringify(patient));
        localStorage.setItem('patientId', p.pid);
        return patient;
      }));
  }

}
