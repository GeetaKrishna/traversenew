import { Component, OnInit } from '@angular/core';
import { CarService } from '../service/carservice';
import { EventService } from '../service/eventservice';
import { Car } from '../domain/car';
import { MenuItem } from 'primeng/primeng';
import { BreadcrumbService } from '../../breadcrumb.service';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { CalendarService } from 'src/app/services/calendar.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
    templateUrl: './dashboard.component.html'
})
export class DashboardDemoComponent implements OnInit {

    cars: Car[];

    cols: any[];

    chartData: any;

    chartOptions: any;

    events: any[];

    selectedCar: Car;

    items: MenuItem[];

    fullCalendarOptions: any;

    constructor(public carService: CarService, public eventService: EventService,
        public breadcrumbService: BreadcrumbService, public calendar: CalendarService, public authentication: LoginService) {
        this.breadcrumbService.setItems([
            { label: 'Dashboard', routerLink: [''] }
        ]);
    }

    ngOnInit() {
        this.authentication
            .getUserId(localStorage.getItem('userName'))
            .subscribe((data) => {

                this.authentication.getPatientByUserId().subscribe((patient) => {
                    this.calendar.getAppointments().subscribe((data1) => {
                        console.log(data1);

                    }, (err) => {
                        console.log(err);
                    });
                });


                // if (data.role == 'Admin') {
                //     this.router.navigate(['administrator/']);
                // } else if (data.role == 'Consumer') {
                //     this.authenticationService
                //         .getPatientByUserId(localStorage.getItem('userId'))
                //         .subscribe((patientData) => {
                //             this.router.navigate(['customer/']);
                //         });
                // } else if (data.role == 'Provider') {
                //     this.authenticationService
                //         .getPatientByUserId(localStorage.getItem('userId'))
                //         .subscribe((patientData) => {
                //             this.router.navigate(['provider/']);
                //         });
                // }
            });

        this.carService.getCarsMedium().then(cars => this.cars = cars);

        this.cols = [
            { field: 'vin', header: 'Vin' },
            { field: 'year', header: 'Year' },
            { field: 'brand', header: 'Brand' },
            { field: 'color', header: 'Color' }
        ];

        this.eventService.getEvents().then(events => { this.events = events; });

        this.chartData = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'Sales',
                data: [12, 19, 3, 5, 2, 3, 9],
                borderColor: [
                    '#7E57C2',
                ],
                borderWidth: 3,
                borderDash: [5, 5],
                fill: false,
                pointRadius: 3
            }, {
                label: 'Income',
                data: [1, 2, 5, 3, 12, 7, 15],
                backgroundColor: [
                    'rgba(187,222,251,0.2)',
                ],
                borderColor: [
                    '#42A5F5',
                ],
                borderWidth: 3,
                fill: true
            },
            {
                label: 'Expenses',
                data: [7, 12, 15, 5, 3, 13, 21],
                borderColor: [
                    '#FFB300',
                ],
                borderWidth: 3,
                fill: false,
                pointRadius: [4, 6, 4, 12, 8, 0, 4]
            },
            {
                label: 'New Users',
                data: [3, 7, 2, 17, 15, 13, 19],
                borderColor: [
                    '#66BB6A',
                ],
                borderWidth: 3,
                fill: false
            }]
        };

        this.chartOptions = {
            responsive: true,
            hover: {
                mode: 'index'
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        };

        this.items = [
            { label: 'Save', icon: 'pi pi-fw pi-check' },
            { label: 'Update', icon: 'pi pi-fw pi-refresh' },
            { label: 'Delete', icon: 'pi pi-fw pi-trash' }
        ];

        this.fullCalendarOptions = {
            plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
            defaultDate: '2016-01-12',
            header: {
                right: 'prev,next, today',
                left: 'title'
            }
        };
    }
}
