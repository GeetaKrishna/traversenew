import { Component, OnInit, ViewChild, AfterViewInit, Renderer2 } from '@angular/core';
import { AppComponent } from '../app.component';
import { ScrollPanel } from 'primeng/primeng';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends AppComponent implements OnInit, AfterViewInit {
  @ViewChild('layoutMenuScroller', { static: true }) public layoutMenuScrollerViewChild: ScrollPanel;

  constructor(public renderer: Renderer2) {
    super(renderer);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => { this.layoutMenuScrollerViewChild.moveBar(); }, 100);
  }

  onMenuClick($event) {
    this.menuClick = true;
    this.resetMenu = false;

    if (!this.isHorizontal()) {
        setTimeout(() => { this.layoutMenuScrollerViewChild.moveBar(); }, 450);
    }
}

}
