import { Component, OnInit } from '@angular/core';
import { DashboardDemoComponent } from '../demo/view/dashboarddemo.component';
import { CarService } from '../demo/service/carservice';
import { EventService } from '../demo/service/eventservice';
import { BreadcrumbService } from '../breadcrumb.service';
import { CalendarService } from '../services/calendar.service';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent extends DashboardDemoComponent {

  constructor(public carServ: CarService, public eve: EventService,
              public bread: BreadcrumbService, public calendar: CalendarService, public authentication: LoginService) {
    super(carServ, eve, bread, calendar, authentication);
  }

}
